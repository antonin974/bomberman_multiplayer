#include <iostream>
#include <time.h>

#include <SFML/Graphics.hpp>

#include "Player.h"

#define MAX_PLAYERS 4
#define BACKGROUND_X 21
#define BACKGROUND_Y 13
#define SPRITE_SCALE 4
#define WINDOW_X 336 * SPRITE_SCALE
#define WINDOW_Y 208 * SPRITE_SCALE

using namespace std;
using namespace sf;

//Gestion des variables
int backgroundMap[BACKGROUND_X][BACKGROUND_Y] = {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2},

    {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},

    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2}};

int foregroundMap[BACKGROUND_X][BACKGROUND_Y] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},

    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},

    {0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},

    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},

    {0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0},

    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},

    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

int bombMap[BACKGROUND_X][BACKGROUND_Y][2];

int explosionMap[BACKGROUND_X][BACKGROUND_Y];

int gameType = 1;

Player playerA;
Player playerB;

Texture explosionTexture, grassTexture, wallTexture, wallBorderTexture, boxTexture, playerAFrontTexture, playerABackTexture, playerALeftTexture, playerARightTexture, playerBFrontTexture, playerBBackTexture, playerBLeftTexture, playerBRightTexture, playerCTexture, playerDTexture, bombTexture, extraBombTexture, healTexture, rangeUpTexture, reductionExplodeTexture;
Sprite explosionSprite, grassSprite, wallSprite, wallBorderSprite, boxSprite, playerAFrontSprite, playerABackSprite, playerALeftSprite, playerARightSprite, playerBFrontSprite, playerBBackSprite, playerBLeftSprite, playerBRightSprite, playerCSprite, playerDSprite, bombSprite, extraBombSprite, healSprite, rangeUpSprite, reductionExplodeSprite;

RenderWindow mainWindow(VideoMode(WINDOW_X, WINDOW_Y), "Alien Bombs Fight v1.3");

//Gestion des fonctions
void powerUp(int positionX, int positionY) {

    // 1 = nbBombes | 2 = RangeBombes | 3 = nbLive | 4 = explodeTime
    switch(foregroundMap[positionX][positionY]) {
        case 8:

            //S�lection du joueur
            switch (bombMap[positionX][positionY][1]) {
                case Player::PlayerA:
                    cout << "NBBombe PA++" << endl;
                    playerA.setNbBomb(playerA.getNbBomb() + 1);
                    break;

                case Player::PlayerB:
                    cout << "NBBombe Pb++" << endl;
                    playerB.setNbBomb(playerB.getNbBomb() + 1);
                    break;
            }

            break;

        case 9:
            cout << "range++" << endl;

            //S�lection du joueur
            switch (bombMap[positionX][positionY][1]) {
                case Player::PlayerA:
                    playerA.setRangeBomb(playerA.getRangeBomb() + 1);
                    break;

                case Player::PlayerB:
                    playerB.setRangeBomb(playerB.getRangeBomb() + 1);
                    break;
            }

            break;

        case 10:
            cout << "lives++" << endl;

            //S�lection du joueur
            switch (bombMap[positionX][positionY][1]) {
                case Player::PlayerA:
                    playerA.setNbLives(playerA.getNbLives() + 1);
                    break;

                case Player::PlayerB:
                    playerB.setNbLives(playerB.getNbLives() + 1);
                    break;
            }

            break;

        case 11:

            //S�lection du joueur
            switch (bombMap[positionX][positionY][1]) {

                case Player::PlayerA:
                    if (playerA.getDelayBomb() > 1) {
                        playerA.setDelayBomb(playerA.getDelayBomb() - 1);
                    }

                    break;

                case Player::PlayerB:
                    if (playerB.getDelayBomb() > 1) {
                        playerB.setDelayBomb(playerB.getDelayBomb() - 1);
                    }

                    break;
            }

            break;
    }

}

void lootOrNot(int bombX, int bombY) {
    int lootRate = rand() % 100 + 1;

    //Drop de Bomb++
	if (lootRate <= 8) {
        foregroundMap[bombX][bombY] = 8;

	//Drop de Range++
	} else if (lootRate <= 18) {
        foregroundMap[bombX][bombY] = 9;

	//Drop de Timer++
	} else if (lootRate <= 25) {
        foregroundMap[bombX][bombY] = 10;

	//Drop de Life++
	} else if (lootRate <= 29) {
        foregroundMap[bombX][bombY] = 11;

	//Pas de drop
	} else {
        foregroundMap[bombX][bombY] = 7;
	}
}

void cleanUpExplosions() {
    while (true) {
        for (int i = 0; i < BACKGROUND_X; i++) {
            for (int j = 0; j < BACKGROUND_Y; j++) {

                if (explosionMap[i][j] != 0) {
                    explosionMap[i][j]--;

                //Les cases contenant des explosions sont nettoy�es
                } else if (!(foregroundMap[i][j] == 8 || foregroundMap[i][j] == 9 || foregroundMap[i][j] == 10 || foregroundMap[i][j] == 11 || foregroundMap[i][j] == 1 || foregroundMap[i][j] == 2 || foregroundMap[i][j] == 3 || foregroundMap[i][j] == 4 || foregroundMap[i][j] == 5 || foregroundMap[i][j] == 6)) {
                    foregroundMap[i][j] = 0;
                }
            }
        }

        sleep(milliseconds(550));
    }
}

void drawGraphicalsArray() {
    //Affichage de background
    for (int i = 0; i < 21; i++) {
        for (int j = 0; j < 13; j++) {
            switch (backgroundMap[i][j]) {
                case 0:
                    grassSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    grassSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(grassSprite);
                    break;

                case 1:
                    wallSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    wallSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(wallSprite);
                    break;

                case 2:
                    wallBorderSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    wallBorderSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(wallBorderSprite);
                    break;
            }
        }
    }

    //Affichage du foreground
    for (int i = 0; i < 21; i++) {
        for (int j = 0; j < 13; j++) {

            switch (foregroundMap[i][j]) {

                case 1:
                    boxSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    boxSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(boxSprite);
                    break;

                case 2:
                    if (playerB.getAliveState()) {
                        //Switch selon l'orientation du personnage
                        switch (playerA.getPlayerDirection()) {
                            case Player::Up:
                                playerABackSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerABackSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerABackSprite);
                                break;

                            case Player::Down:
                                playerAFrontSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerAFrontSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerAFrontSprite);
                                break;

                            case Player::Right:
                                playerARightSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerARightSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerARightSprite);
                                break;

                            case Player::Left:
                                playerALeftSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerALeftSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerALeftSprite);
                                break;
                        }
                    }

                    break;

                case 3:
                    if (playerB.getAliveState()) {
                        //Switch selon l'orientation du personnage
                        switch (playerB.getPlayerDirection()) {
                            case Player::Up:
                                playerBBackSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerBBackSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerBBackSprite);
                                break;

                            case Player::Down:
                                playerBFrontSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerBFrontSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerBFrontSprite);
                                break;

                            case Player::Right:
                                playerBRightSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerBRightSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerBRightSprite);
                                break;

                            case Player::Left:
                                playerBLeftSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                                playerBLeftSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                                mainWindow.draw(playerBLeftSprite);
                                break;
                        }
                    }

                    break;

                case 4:
                    playerCSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    playerCSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(playerCSprite);
                    break;

                case 5:
                    playerDSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    playerDSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(playerDSprite);
                    break;

                case 6:
                    if (bombMap[i][j][0] != -1) {
                        bombSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                        bombSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        mainWindow.draw(bombSprite);
                    }
                    break;

                case 7:
                    explosionSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    explosionSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(explosionSprite);
                    break;

                case 8:
                    extraBombSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    extraBombSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(extraBombSprite);
                    break;

                case 9:
                    rangeUpSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    rangeUpSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(rangeUpSprite);
                    break;

                case 10:
                    healSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    healSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(healSprite);
                    break;

                case 11:
                    reductionExplodeSprite.setPosition(SPRITE_SCALE * 16 * i, SPRITE_SCALE * 16 * j);
                    reductionExplodeSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                    mainWindow.draw(reductionExplodeSprite);
                    break;
            }
        }
    }
}

void fillTextures() {
    grassTexture.loadFromFile("assets/grass.png");
    wallTexture.loadFromFile("assets/wall1.png");
    wallBorderTexture.loadFromFile("assets/wall2.png");
    boxTexture.loadFromFile("assets/box.png");
    bombTexture.loadFromFile("assets/bomb.png");
    explosionTexture.loadFromFile("assets/explosion.png");

    extraBombTexture.loadFromFile("assets/extraBomb.png");
    healTexture.loadFromFile("assets/heal.png");
    rangeUpTexture.loadFromFile("assets/range.png");
    reductionExplodeTexture.loadFromFile("assets/explodeReduction.png");

    playerAFrontTexture.loadFromFile("assets/playerA_front.png");
    playerABackTexture.loadFromFile("assets/playerA_back.png");
    playerALeftTexture.loadFromFile("assets/playerA_left.png");
    playerARightTexture.loadFromFile("assets/playerA_right.png");

    playerBFrontTexture.loadFromFile("assets/playerB_front.png");
    playerBBackTexture.loadFromFile("assets/playerB_back.png");
    playerBLeftTexture.loadFromFile("assets/playerB_left.png");
    playerBRightTexture.loadFromFile("assets/playerB_right.png");

    playerCTexture.loadFromFile("assets/playerC_front.png");
    playerDTexture.loadFromFile("assets/playerD_front.png");
}

void settingUpExplosions(int bombX, int bombY) {
    int maxRange = 1;

    //Switch g�rant la port�e max du joueur
    switch(bombMap[bombX][bombY][1]) {
        case Player::PlayerA:
            maxRange = playerA.getRangeBomb();
            break;

        case Player::PlayerB:
            maxRange = playerB.getRangeBomb();
            break;
    }

    //Explosions du haut
    for (int i = 1; i <= maxRange; i++) {

        //S'il y a un mur du background
        if (backgroundMap[bombX][bombY - i] == 2 || backgroundMap[bombX][bombY - i] == 1) {
            explosionMap[bombX][bombY - i] = 1;
            break;

        //S'il y a une caisse
        } else if (foregroundMap[bombX][bombY - i] == 1){
            lootOrNot(bombX, bombY - i);
            explosionMap[bombX][bombY - i] = 1;
            break;

        //S'il y a du vide
        } else if (foregroundMap[bombX][bombY - i] == 0) {
            foregroundMap[bombX][bombY - i] = 7;
            explosionMap[bombX][bombY - i] = 1;

        //S'il y a un joueur
        } else if (foregroundMap[bombX][bombY - i] == 2 || foregroundMap[bombX][bombY - i] == 3 || foregroundMap[bombX][bombY - i] == 4 || foregroundMap[bombX][bombY - i] == 5) {

            //Selon le joueur dans l'explosion
            if (foregroundMap[bombX][bombY - i] == 2) {
                playerA.setAliveState(false);
                cout << "C'est le joueur A haut" << endl;
            } else if (foregroundMap[bombX][bombY - i] == 3) {
                cout << "C'est le joueur B haut" << endl;
                playerB.setAliveState(false);
            }

            foregroundMap[bombX][bombY - i] = 7;
            explosionMap[bombX][bombY - i] = 1;
        }
    }

    //Explosions du bas
    for (int i = 1; i <= maxRange; i++) {

        //S'il y a un mur du background
        if (backgroundMap[bombX][bombY + i] == 2 || backgroundMap[bombX][bombY + i] == 1) {
            explosionMap[bombX][bombY + i] = 1;
            break;

        //S'il y a une caisse
        } else if (foregroundMap[bombX][bombY + i] == 1){
            lootOrNot(bombX, bombY + i);
            explosionMap[bombX][bombY + i] = 1;
            break;

        //S'il y a du vide
        } else if (foregroundMap[bombX][bombY + i] == 0) {
            foregroundMap[bombX][bombY + i] = 7;
            explosionMap[bombX][bombY + i] = 1;

        //S'il y a un joueur
        } else if (foregroundMap[bombX][bombY + i] == 2 || foregroundMap[bombX][bombY + i] == 3 || foregroundMap[bombX][bombY + i] == 4 || foregroundMap[bombX][bombY + i] == 5) {

            //Selon le joueur dans l'explosion
            if (foregroundMap[bombX][bombY + i] == 2) {
                cout << "C'est le joueur A bas" << endl;
                playerA.setAliveState(false);
            } else if (foregroundMap[bombX][bombY + i] == 3) {
                cout << "C'est le joueur B bas" << endl;
                playerB.setAliveState(false);
            }

            foregroundMap[bombX][bombY + i] = 7;
            explosionMap[bombX][bombY + i] = 1;
        }
    }

    //Explosions de la gauche
    for (int i = 1; i <= maxRange; i++) {

        //S'il y a un mur du background
        if (backgroundMap[bombX - i][bombY] == 2 || backgroundMap[bombX - i][bombY] == 1) {
            explosionMap[bombX - i][bombY] = 1;
            break;

        //S'il y a une caisse
        } else if (foregroundMap[bombX - i][bombY] == 1){
            lootOrNot(bombX - i, bombY);
            explosionMap[bombX - i][bombY] = 1;
            break;

        //S'il y a du vide
        } else if (foregroundMap[bombX - i][bombY] == 0) {
            foregroundMap[bombX - i][bombY] = 7;
            explosionMap[bombX - i][bombY] = 1;

        //S'il y a un joueur
        } else if (foregroundMap[bombX - i][bombY] == 2) {
            playerA.setAliveState(false);
            cout << "C'est le joueur A gauche" << endl;
        } else if (foregroundMap[bombX - i][bombY] == 3) {
            playerB.setAliveState(false);
            cout << "C'est le joueur B gauche" << endl;
        }

        foregroundMap[bombX - i][bombY] = 7;
        explosionMap[bombX - i][bombY] = 1;
    }

    //Explosions de la droite
    for (int i = 1; i <= maxRange; i++) {

        //S'il y a un mur du background
        if (backgroundMap[bombX + i][bombY] == 2 || backgroundMap[bombX + i][bombY] == 1) {
            explosionMap[bombX + i][bombY] = 1;
            break;

        //S'il y a une caisse
        } else if (foregroundMap[bombX + i][bombY] == 1){
            lootOrNot(bombX + i, bombY);
            explosionMap[bombX + i][bombY] = 1;
            break;

        //S'il y a du vide
        } else if (foregroundMap[bombX + i][bombY] == 0) {
            foregroundMap[bombX + i][bombY] = 7;
            explosionMap[bombX + i][bombY] = 1;

        //S'il y a un joueur
        } else if (foregroundMap[bombX + i][bombY] == 2) {
                cout << "C'est le joueur A droite" << endl;
                playerA.setAliveState(false);
            } else if (foregroundMap[bombX + i][bombY] == 3) {
                cout << "C'est le joueur B droite" << endl;
                playerB.setAliveState(false);
            }

            foregroundMap[bombX][bombY + i] = 7;
            explosionMap[bombX][bombY + i] = 1;
        }

    //Mise en place d'une explosion � la place de la bombe
    foregroundMap[bombX][bombY] = 7;
    explosionMap[bombX][bombY] = 1;
}

void fillSprites() {
    grassSprite.setTexture(grassTexture);
    wallBorderSprite.setTexture(wallBorderTexture);
    wallSprite.setTexture(wallTexture);
    boxSprite.setTexture(boxTexture);
    bombSprite.setTexture(bombTexture);
    explosionSprite.setTexture(explosionTexture);

    extraBombSprite.setTexture(extraBombTexture);
    healSprite.setTexture(healTexture);
    rangeUpSprite.setTexture(rangeUpTexture);
    reductionExplodeSprite.setTexture(reductionExplodeTexture);

    playerAFrontSprite.setTexture(playerAFrontTexture);
    playerABackSprite.setTexture(playerABackTexture);
    playerALeftSprite.setTexture(playerALeftTexture);
    playerARightSprite.setTexture(playerARightTexture);

    playerBFrontSprite.setTexture(playerBFrontTexture);
    playerBBackSprite.setTexture(playerBBackTexture);
    playerBLeftSprite.setTexture(playerBLeftTexture);
    playerBRightSprite.setTexture(playerBRightTexture);

    playerCSprite.setTexture(playerCTexture);
    playerDSprite.setTexture(playerDTexture);
}

void bombTimer() {
    while (true) {
      for (int i = 0; i < BACKGROUND_X; i++) {
            for (int j = 0; j < BACKGROUND_Y; j++) {
                if (bombMap[i][j][0] != -1) {
                    bombMap[i][j][0]--;

                    if (bombMap[i][j][0] == -1) {
                        foregroundMap[i][j] = 0;
                        settingUpExplosions(i, j);
                    }
                }

                if (bombMap[i][j][0] == -1) {

                    //Switch qui s�lectionne le joueur qui a pos� la bombe
                    switch (bombMap[i][j][1]) {

                        //JoueurA
                        case Player::PlayerA:
                            bombMap[i][j][1] = -1;

                            if (playerA.getNbBombPut() > 0) {
                                playerA.setNbBombPut(playerA.getNbBombPut() - 1);
                            }

                            break;

                        //JoueurB
                        case Player::PlayerB:
                            bombMap[i][j][1] = -1;

                            if (playerB.getNbBombPut() > 0) {
                                playerB.setNbBombPut(playerB.getNbBombPut() - 1);
                            }

                            break;
                    }
                }
            }
        }

        sleep(seconds(1));
    }
}

void fillBombMap() {
    for (int i = 0; i < BACKGROUND_X; i++) {
        for (int j = 0; j < BACKGROUND_Y; j++) {
            bombMap[i][j][0] = -1;
            bombMap[i][j][1] = -1;
        }
    }
}

int main() {
    //Modification de la g�n�ration al�atoire
    srand(time(NULL));

    //Remplissage du tableaux des bombes
    fillBombMap();

    //Cr�ation et lancement du thread du timer pour les bombes
    Thread bombManagerThread(&bombTimer);

    bombManagerThread.launch();

    //Cr�ation et lancement du thread des explosions
    Thread explosionsManagerThread(&cleanUpExplosions);

    explosionsManagerThread.launch();

    //Mise en place des textures et des sprites
    fillTextures();
    fillSprites();

    //S�lection des modes de jeu
    if (gameType == 1) {
        playerA.setPosPlayer(1, 1);
        playerB.setPosPlayer(19, 11);

        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
    }

    Event actualEvent;

    //Boucle de jeu principale
    while (mainWindow.isOpen()) {

        //Tant qu'il y a des �venements en attente
        while (mainWindow.pollEvent(actualEvent)) {
            //cout << "PlayerA : " << foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] << " Vie : " << playerA.getAliveState() << endl;
            //cout << "PlayerB : " << foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] << " Vie : " << playerB.getAliveState() << endl << endl;

            switch (actualEvent.type) {

                //Quand la fen�tre est ferm�e
                case Event::Closed:
                    mainWindow.close();

                    return EXIT_SUCCESS;

                //Si on appuie sur une touche
                case Event::KeyPressed:

                    //On teste la touche press�e
                    switch (actualEvent.key.code) {

                    //PlayerA
                        //Touche Z
                        case Keyboard::Z:
                            if (playerA.getAliveState()) {
                                if (foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] == 8 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] == 9 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] == 10 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] == 11) {
                                    bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1][1] = Player::PlayerA;
                                    powerUp(playerA.getPosXPlayer(), playerA.getPosYPlayer() - 1);
                                }

                                if (backgroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] != 1 && backgroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] != 2 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] != 6 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] != 1 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() - 1] != 7) {
                                    if (playerA.getThrewBombState()) {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 6;
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][0] = playerA.getDelayBomb();
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                        playerA.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 0;
                                    }

                                    playerA.setPosPlayer(playerA.getPosXPlayer(), playerA.getPosYPlayer() - 1);
                                    foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
                                }

                                playerA.setPlayerDirection(Player::Up);
                            }

                            break;

                        //Touche Q
                        case Keyboard::Q:
                            if (playerA.getAliveState()) {
                                if (foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] == 8 || foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] == 9 || foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] == 10 || foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] == 11) {
                                    bombMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                    powerUp(playerA.getPosXPlayer() - 1, playerA.getPosYPlayer());
                                }

                                if (backgroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] != 1 && backgroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] != 2 && foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] != 6 && foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] != 1 && foregroundMap[playerA.getPosXPlayer() - 1][playerA.getPosYPlayer()] != 7) {
                                    if (playerA.getThrewBombState()) {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 6;
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][0] = playerA.getDelayBomb();
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                        playerA.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 0;
                                    }

                                    playerA.setPosPlayer(playerA.getPosXPlayer() - 1, playerA.getPosYPlayer());
                                    foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
                                }

                                playerA.setPlayerDirection(Player::Left);
                            }

                            break;

                        //Touche S
                        case Keyboard::S:
                            if (playerA.getAliveState()) {
                                if (foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] == 8 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] == 9 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] == 10 || foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] == 11) {
                                    bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1][1] = Player::PlayerA;
                                    powerUp(playerA.getPosXPlayer(), playerA.getPosYPlayer() + 1);
                                }

                                if (backgroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] != 1 && backgroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] != 2 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] != 6 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] != 1 && foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer() + 1] != 7) {
                                    if (playerA.getThrewBombState()) {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 6;
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][0] = playerA.getDelayBomb();
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                        playerA.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 0;
                                    }

                                    playerA.setPosPlayer(playerA.getPosXPlayer(), playerA.getPosYPlayer() + 1);
                                    foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
                                }

                                playerA.setPlayerDirection(Player::Down);
                            }

                            break;

                        //Touche D
                        case Keyboard::D:
                            if (playerA.getAliveState()) {
                                if (foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] == 8 || foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] == 9 || foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] == 10 || foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] == 11) {
                                    bombMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                    powerUp(playerA.getPosXPlayer() + 1, playerA.getPosYPlayer());
                                }

                                if (backgroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] != 1 && backgroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] != 2 && foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] != 6 && foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] != 1 && foregroundMap[playerA.getPosXPlayer() + 1][playerA.getPosYPlayer()] != 7) {
                                    if (playerA.getThrewBombState()) {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 6;
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][0] = playerA.getDelayBomb();
                                        bombMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()][1] = Player::PlayerA;
                                        playerA.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 0;
                                    }

                                    playerA.setPosPlayer(playerA.getPosXPlayer() + 1, playerA.getPosYPlayer());
                                    foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
                                }

                                playerA.setPlayerDirection(Player::Right);
                            }

                            break;

                        //Touche Espace
                        case Keyboard::Space:
                            if (playerA.getNbBomb() - playerA.getNbBombPut() > 0 && playerA.getAliveState()) {
                                playerA.setThrewBombState(true);
                                playerA.setNbBombPut(playerA.getNbBombPut() + 1);
                            }

                            break;

                    //PlayerB
                        //Touche Haut
                        case Keyboard::Up:
                            if (playerB.getAliveState()) {
                                if (foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] == 8 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] == 9 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] == 10 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] == 11) {
                                    bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1][1] = Player::PlayerB;
                                    powerUp(playerB.getPosXPlayer(), playerB.getPosYPlayer() - 1);
                                }

                                if (backgroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] != 1 && backgroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] != 2 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] != 6 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] != 1 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() - 1] != 7) {
                                    if (playerB.getThrewBombState()) {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 6;
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][0] = playerB.getDelayBomb();
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                        playerB.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 0;
                                    }

                                    playerB.setPosPlayer(playerB.getPosXPlayer(), playerB.getPosYPlayer() - 1);
                                    foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
                                }

                                playerB.setPlayerDirection(Player::Up);
                            }

                            break;

                        //Touche Gauche
                        case Keyboard::Left:
                            if (playerB.getAliveState()) {
                                if (foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] == 8 || foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] == 9 || foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] == 10 || foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] == 11) {
                                   bombMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                   powerUp(playerB.getPosXPlayer() - 1, playerB.getPosYPlayer());
                                }

                                if (backgroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] != 1 && backgroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] != 2 && foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] != 6 && foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] != 1 && foregroundMap[playerB.getPosXPlayer() - 1][playerB.getPosYPlayer()] != 7) {
                                    if (playerB.getThrewBombState()) {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 6;
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][0] = playerB.getDelayBomb();
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                        playerB.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 0;
                                    }

                                    playerB.setPosPlayer(playerB.getPosXPlayer() - 1, playerB.getPosYPlayer());
                                    foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
                                }

                                playerB.setPlayerDirection(Player::Left);
                            }

                            break;

                        //Touche Bas
                        case Keyboard::Down:
                            if (playerB.getAliveState()) {
                                if (foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] == 8 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] == 9 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] == 10 || foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] == 11) {
                                    bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1][1] = Player::PlayerB;
                                    powerUp(playerB.getPosXPlayer(), playerB.getPosYPlayer() + 1);
                                }

                                if (backgroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] != 1 && backgroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] != 2 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] != 6 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] != 1 && foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer() + 1] != 7) {
                                   if (playerB.getThrewBombState()) {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 6;
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][0] = playerB.getDelayBomb();
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                        playerB.setThrewBombState(false);
                                    } else {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 0;
                                    }

                                    playerB.setPosPlayer(playerB.getPosXPlayer(), playerB.getPosYPlayer() + 1);
                                    foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
                                }

                                playerB.setPlayerDirection(Player::Down);
                            }

                            break;

                        //Touche Droite
                        case Keyboard::Right:
                            if (playerB.getAliveState()) {
                                if (foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] == 8 || foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] == 9 || foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] == 10 || foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] == 11) {
                                   bombMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                   powerUp(playerB.getPosXPlayer() + 1, playerB.getPosYPlayer());
                                }

                                if (backgroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] != 1 && backgroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] != 2 && foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] != 6 && foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] != 1 && foregroundMap[playerB.getPosXPlayer() + 1][playerB.getPosYPlayer()] != 7) {
                                    if (playerB.getThrewBombState()) {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 6;
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][0] = playerB.getDelayBomb();
                                        bombMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()][1] = Player::PlayerB;
                                        playerB.setThrewBombState(false);

                                    } else {
                                        foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 0;
                                    }

                                    playerB.setPosPlayer(playerB.getPosXPlayer() + 1, playerB.getPosYPlayer());
                                    foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
                                }

                                playerB.setPlayerDirection(Player::Right);
                            }

                            break;

                        //Touche Ctrl
                        case Keyboard::RControl:
                            if (playerB.getNbBomb() - playerB.getNbBombPut() > 0 && playerB.getAliveState()) {
                                playerB.setThrewBombState(true);
                                playerB.setNbBombPut(playerB.getNbBombPut() + 1);
                            }

                            break;
                    }
            }
        }

        //Clean de la fen�tre
        mainWindow.clear();

        //Actualisation des joueurs
        if (foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] != 6) {
            if (playerA.getAliveState()) {
                foregroundMap[playerA.getPosXPlayer()][playerA.getPosYPlayer()] = 2;
            }

            if (playerB.getAliveState()) {
                foregroundMap[playerB.getPosXPlayer()][playerB.getPosYPlayer()] = 3;
            }
        }

        //Drawing graphicals arrays
        drawGraphicalsArray();

        //Updating screen
        mainWindow.display();
    }

    return EXIT_SUCCESS;
}
