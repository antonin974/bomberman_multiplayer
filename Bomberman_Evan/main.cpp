#include <iostream>
#include <SFML/Graphics.hpp>

#define SPRITE_SCALE 4
#define WINDOW_X 16 * 21 * SPRITE_SCALE
#define WINDOW_Y 13 * 16 * SPRITE_SCALE
#define BACKGROUND_X 21
#define BACKGROUND_Y 13

using namespace std;
using namespace sf;

int main() {
    int backgroundMap[BACKGROUND_Y][BACKGROUND_X] = {
        {1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {1 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {1 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {1 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {1 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {1 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 , 2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,2 ,0 ,1},

        {1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1},

        {2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2}};

    int foregroundMap[BACKGROUND_Y][BACKGROUND_X] = {
        {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0},

        {0 ,2 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,3 ,0},

        {0 ,0 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,0 ,0},

        {0 ,1 ,1 ,1 ,1 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ,1 ,1 ,1 ,1 ,0},

        {0 ,1 ,0 ,1 ,0 ,0 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,0 ,0 ,1 ,0 ,1 ,0},

        {0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0},

        {0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0},

        {0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0},

        {0 ,1 ,0 ,1 ,0 ,0 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,0 ,0 ,1 ,0 ,1 ,0},

        {0 ,1 ,1 ,1 ,1 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,0 ,1 ,1 ,1 ,1 ,0},

        {0 ,0 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,1 ,0 ,0 ,0},

        {0 ,4 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,5 ,0},

        {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}};

    RenderWindow app(VideoMode(WINDOW_X, WINDOW_Y), "SFML");

    Texture grassTexture, wallTexture, wallBorderTexture;

    if (!grassTexture.loadFromFile("assets/grass.png")) {
        return EXIT_FAILURE;
    }

    if (!wallTexture.loadFromFile("assets/wall1.png")) {
        return EXIT_FAILURE;
    }

    if (!wallBorderTexture.loadFromFile("assets/wall2.png")) {
        return EXIT_FAILURE;
    }

    Sprite grassSprite, wallSprite, wallBorderSprite;

    grassSprite.setTexture(grassTexture);
    wallBorderSprite.setTexture(wallBorderTexture);
    wallSprite.setTexture(wallTexture);

    Texture boxTexture, player1Texture, player2Texture, player3Texture, player4Texture, bombTexture;

    if (!boxTexture.loadFromFile("assets/box.png")) {
        return EXIT_FAILURE;
    }

    if (!player1Texture.loadFromFile("assets/player1_stand.png")) {
        return EXIT_FAILURE;
    }

    if (!player2Texture.loadFromFile("assets/player2_stand.png")) {
        return EXIT_FAILURE;
    }

    if (!player3Texture.loadFromFile("assets/player3_stand.png")) {
        return EXIT_FAILURE;
    }

    if (!player4Texture.loadFromFile("assets/player4_stand.png")) {
        return EXIT_FAILURE;
    }

    /*if (!bombTexture.loadFromFile("assets/bomb.png")) {
        return EXIT_FAILURE;
    }*/

    Sprite boxSprite, player1Sprite, player2Sprite, player3Sprite, player4Sprite, bombSprite;

    boxSprite.setTexture(boxTexture);
    player1Sprite.setTexture(player1Texture);
    player2Sprite.setTexture(player2Texture);
    player3Sprite.setTexture(player3Texture);
    player4Sprite.setTexture(player4Texture);

    Event e;

    while (true) {

        app.clear();
        for (int i = 0; i < BACKGROUND_Y; i++) {
            for (int j = 0; j < BACKGROUND_X; j++) {
                switch (backgroundMap[i][j]) {
                    case 0:
                        grassSprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        grassSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(grassSprite);
                        break;

                    case 1:
                        wallSprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        wallSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(wallSprite);
                        break;

                    case 2:
                        wallBorderSprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        wallBorderSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(wallBorderSprite);
                        break;
                }

                switch (foregroundMap[i][j]) {
                    case 1:
                        boxSprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        boxSprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(boxSprite);
                        break;

                    case 2:
                        player1Sprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        player1Sprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(player1Sprite);
                        break;

                    case 3:
                        player2Sprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        player2Sprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(player2Sprite);
                        break;

                    case 4:
                        player3Sprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        player3Sprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(player3Sprite);
                        break;

                    case 5:
                        player4Sprite.setPosition(SPRITE_SCALE *16 * j, SPRITE_SCALE *16 * i);
                        player4Sprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
                        app.draw(player4Sprite);
                        break;
                }
            }
        }
        app.display();
    }

    return 0;
}
